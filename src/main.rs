use std::fs;
use serde::{ Serialize, Deserialize };

#[derive(Serialize, Deserialize, Debug)]
struct Page {
    pages: Vec<PageDetails>,
}

#[derive(Serialize, Deserialize, Debug)]
struct PageDetails {
    title: String,
    link: bool,
    path: String,
    content: String,
    template: bool,
    index: u32,
}

fn main() {

    let json_str = fs::read_to_string("map.json")
        .expect("Error loading json file");

    // println!("{:?}", json_str);

    let pages_obj = serde_json::from_str::<Page>(&json_str)
        .expect("Error serializing JSON to Struct");

    // println!("{:#?}", pages_obj);

    let mut template_str: String = "".to_string();

    pages_obj.pages.iter().for_each(|page| {
        if page.template {
            template_str = fs::read_to_string(&page.content)
                .expect("Error loading HTML Template File")
        }
    });

    // println!("{:?}", template_str);

    let mut menu_items: Vec<String> = Vec::new();
    let mut menu_item_count = 0;

    pages_obj.pages.iter().for_each(|page| {
        if page.index > 0 {
            menu_item_count += 1;
        }
    });

    let mut menu_link_str: String = "".to_string();
    let mut i: u32 = 1;

    loop {
        if menu_items.len() == menu_item_count {
            break;
        } else {
            pages_obj.pages.iter().for_each(|page| {
                if page.index == i && page.link == false {
                    menu_items.push(page.title.to_string());
                    let link_str: String = format!("<a href='{}' class='menu-link'>{}</a>\n", &page.path, &page.title);
                    menu_link_str.push_str(&link_str);
                } else if page.index == i && page.link == true {
                    menu_items.push(page.title.to_string());
                    let link_str: String = format!("<a href='{}' class='menu-link' target='_blank'>{}</a>\n", &page.path, &page.title);
                    menu_link_str.push_str(&link_str);
                }
            });
        }
        i = i + 1;
    }

    // println!("{}", menu_item_count);
    // println!("{:#?}", menu_items);
    println!("{}", menu_link_str);
}
